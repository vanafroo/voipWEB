“Variant Landing Page Theme" Documentation by InkThemes

This theme has been designed for those who want to have a single landing page website.

That's why you will see the landing page by default on the front page when you activated the theme.

This is the primary reason, we have not included the menu on the front, and you don't see it when you are activating the theme.

However, there are some users who would just want to use the theme as a normal blog based theme and need to display core WordPress blog posts on their website.

To allow doing that, the team has come with a blog template, which can be assigned to any page.

That page can then be setup as "To show on the front page" from the Reading settings.

The navigation menu will appear, when you select the blog template as the front page or visit other pages.
 
Once the user creates a new page with Blog template and shows it on the front, the same theme will act like any other default WordPress blog theme.

-----------------------------------------------------------------

Get Your Landing Page Website Ready in Just 1 Click.

Thank you for downloading the Variant Landing Page Theme. The theme is very simple and easy to use. If you have any questions that are beyond the scope of this readme file, please feel free to ask questions at the InkThemes Support Forum. Follow the link given below:

http://www.inkthemes.com/community/

More about the theme usage:

1. Beginning (Important)

A) Installing the theme

To be able to use the “Variant Wordpress Theme, you need to install WordPress on your server. If you don't know how to do it, then click on the link below that will guide you how to install WordPress.
 
http://www.inkthemes.com/12-simple-steps-to-install-wordpress-locally/01/

To install the “Variant WordPress theme, just place the theme folder “variant-landing-page”  in the themes directory under wp-content and activate it from the WordPress admin. As soon as you do that, the theme will be activated on your Website.\

2. Change the Landing Page Templates

Just log in to your WordPress Dashboard. There from theme options panel, you can easily select the desired skin and change the Landing page template.

Once again, thank you so much for trying the “Variant Landing Page Theme. As we said at the beginning, we will be glad to help you. If you have any questions related to this theme then do let us know & we will try our best to assist. If you have any general questions related to the themes on InkThemes, we would recommend you to visit the InkThemes Support Forum and ask your queries there.

3. Integrating lead form:

a) Install the FormGet contact form plugin.
b) Go to Contact Form page from the admin menu.
c) Under the page, Create a form as you desired and go to the dashboard page by clicking the dashboard menu.
d) In dashboard page, you will see your form. Click on Embed Form button.
e) A page will be opened, then click on the Share Form Link menu link in the left sidebar.
f) Now copy the link from text field, then paste it into the Lead form setting via customizer.
g) Now save the settings, Your form would be shown on the landing page.


Theme Resources and their license:
------------------------------------

Images: All images are GPL compatible.
    * All Images are created by InkThemes.com, that have gpl license.
    * template_one_top_bg.png :=> Source: https://pixabay.com/en/pool-spa-wellness-hotel-resort-384573/
    * img_thumb1.jpg :=> https://pixabay.com/en/bridge-london-tower-bridge-898114/
    * img_thumb2.jpg :=> https://pixabay.com/en/london-eye-ferris-wheel-london-945497/
    * img_thumb3.jpg :=> https://pixabay.com/en/lighthouse-navigation-beacon-tower-93487/
    * img_thumb4.jpg :=> https://pixabay.com/en/temple-lotus-temple-architecture-93446/
 
Js: All js file have GPL compatible.
    * variantmenu.js :=> Created by InkThemes that have gpl compatible
    * meanmenu.js :=> Source: https://github.com/meanthemes/meanMenu/blob/master/jquery.meanmenu.js
    * superfish.js :=> Source: http://users.tpg.com.au/j_birch/plugins/superfish/

Fonts: Google Font
    * Raleway :=> Source: http://fonts.googleapis.com/css?family=Raleway:500,600,700,100,800,900,400,200,300

Fontawesome:
    * Source: http://fortawesome.github.io/Font-Awesome/
    * License: http://fortawesome.github.io/Font-Awesome/license/

Css Framework Bootstrap:
    * Source: http://getbootstrap.com/
    * License: https://github.com/twbs/bootstrap/blob/master/LICENSE

