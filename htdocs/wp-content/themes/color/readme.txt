Color WordPress Theme, Copyright (C) 2015, megathemes.com
Color is distributed under the terms of the GNU GPL

@Documentation
http://www.megathemes.com/docs/lite

@SupportForums
http://www.megathemes.com/forums

@ChangeLogs
Mega: http://www.megathemes.com/changelogs/mega
Theme: http://www.megathemes.com/changelogs/

@Licenses

===Ionicons===
by Ben Sperry
License: MIT
twitter.com/benjsperry


===Font Awesome===
by @davegandy
Font License: SIL OFL 1.1
CSS License: MIT
fontawesome.io


===jPlayer===
by Happyworm Ltd
License: MIT
jplayer.org


===Bootstrap===
by @fat & @mdo
License: Apache 2


===Jssor===
jssor.com
License: MIT


===Lato Font===
by Lukasz Dziedzic
License: SIL OFL 1.1
https://www.google.com/fonts/specimen/Lato


===Slider Image===
by @modestasu
License: CC0
https://unsplash.com/modestasu


===Holder.js===
by Ivan Malopinsky
License: MIT
holderjs.com


===Hybrid Media Grabber===
Copyright (c) 2008 - 2015, Justin Tadlock